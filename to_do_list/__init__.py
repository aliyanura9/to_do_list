from __future__ import absolute_import, unicode_literals

from to_do_list.tasks import app as celery_app

__all__ = ('celery_app',)