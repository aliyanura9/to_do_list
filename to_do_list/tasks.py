import os

from celery import Celery
from django.core.mail import EmailMessage


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "to_do_list.settings")

app = Celery("board")
app.conf.update(timezone='Asia/Bishkek')
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.task()
def notify_user_about_task(title, email):
    message = f'Ваша задача {title} должна быть выполнена в ближайшее время'
    email = EmailMessage(subject='Напоминание о задаче', body=message, to=[email])
    email.send()

