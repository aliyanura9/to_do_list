from rest_framework import serializers
from board.models import Category, Task, TaskFile


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'title')


class FileSerializer(serializers.ModelSerializer):
    # file = serializers.CharField(source='file.name')
    class Meta:
        model = TaskFile
        fields = ('id', 'file')


class TaskSerializer(serializers.ModelSerializer):
    category_id = serializers.IntegerField()
    category = serializers.CharField(source='category.title')
    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'completion_date',
                  'created_at','completed_at', 'category',
                  'priority', 'category_id', 'is_done', 'comment')
        extra_kwargs = {
            'description': {'required': False},
            'is_done': {'required': False},
            'completed_at': {'required': False},
            'created_at': {'required': False},
            'category': {'required': False},
            'comment': {'required': False},
        }


class TaskDetailsSerializer(serializers.ModelSerializer):
    files = FileSerializer(many=True)
    category = serializers.CharField(source='category.title')
    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'completion_date',
                  'created_at','completed_at', 'category',
                  'priority', 'is_done', 'comment', 'files')