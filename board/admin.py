from django.contrib import admin
from board.models import Task, Category, TaskFile


class FilesInline(admin.TabularInline):
    model = TaskFile


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'user')
    list_display_links = ('title',)


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'priority',
                    'completion_date',
                    'category', 'is_done')
    list_display_links = ('title',)
    list_editable = ('priority', 'category', 'is_done')
    readonly_fields = ('created_at', 'completed_at')
    inlines = [FilesInline]
