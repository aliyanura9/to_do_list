from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    title = models.CharField(max_length=250,
                             verbose_name='Наименование')
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='categories',
                             verbose_name='Пользователь')
    
    def __str__(self):
        return self.title
    
    class Meta:
        db_table = 'categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
        ordering = ('title',)
        unique_together = ('user', 'title')


class Task(models.Model):
    URGENTLY_IMPORTANT = '1'
    URGENTLY = '2'
    IMPORTANT = '3'
    NOT_IMPORTANT = '4'
    PRIORITY_CHOISE = (
        (URGENTLY_IMPORTANT, 'Срочно и важно'),
        (URGENTLY, 'Срочно'),
        (IMPORTANT, 'Важно'),
        (NOT_IMPORTANT, 'Не важно'),
    )
    title = models.CharField(max_length=250, verbose_name='Наименование')
    description = models.TextField(null=True, blank=True,
                                   verbose_name='Описание')
    completion_date = models.DateTimeField(verbose_name="Срок завершения")
    created_at = models.DateTimeField(auto_now=True,
                                      verbose_name='Дата создания')
    completed_at = models.DateTimeField(null=True, blank=True,
                                        verbose_name='Дата завершения')
    priority = models.CharField(max_length=10, choices=PRIORITY_CHOISE,
                                verbose_name='Приоритетность')
    category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                 related_name='tasks',
                                 verbose_name='Категория')
    is_done = models.BooleanField(default=False, verbose_name='Выполнено')
    celery_task_id = models.CharField(max_length=50, null=True, blank=True,
                                      verbose_name='id задачи в селери')
    comment = models.TextField(null=True, blank=True, verbose_name='комментарий')

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'tasks'
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        ordering = ('priority', 'completion_date', 'created_at')


class TaskFile(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE,
                             related_name='files',
                             verbose_name='задание')
    file = models.FileField(max_length=500, upload_to='tasks',
                            verbose_name='файл')

    class Meta:
        db_table = 'files'
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'
