from rest_framework import status
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from django.http.response import HttpResponse
from board.serialisers import CategorySerializer,\
                              TaskSerializer,\
                              TaskDetailsSerializer,\
                              FileSerializer
from board.models import Category, Task, TaskFile
from board.permissions import IsCategoryOwner
from board.services import CategoryService, TaskService,\
                           FileService
from board.filters import TaskFilter


class CategoryViewSet(ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CategorySerializer
    lookup_field = 'pk'

    def get_queryset(self):
        queryset = Category.objects.filter(
            user=self.request.user
        )
        return queryset
    
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        CategoryService.create(
            user=request.user,
            title=serializer.validated_data.get('title')
        )
        return Response(data={
            'message': 'The category has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)


class TaskViewSet(ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = TaskSerializer
    filter_backends = (SearchFilter, DjangoFilterBackend)
    filterset_class = TaskFilter
    search_fields = ('title',)
    queryset = Task.objects.all()
    lookup_field = 'pk'

    
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        TaskService.create(
            title=serializer.validated_data['title'],
            description=serializer.validated_data.get('description', None),
            completion_date=serializer.validated_data['completion_date'],
            priority=serializer.validated_data['priority'],
            category_id=serializer.validated_data['category_id'],
            comment=serializer.validated_data.get('comment', None),
            user=request.user
        )
        return Response(data={
            'message': 'The task has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        task = TaskService.get(id=kwargs.get('pk'))
        data = TaskDetailsSerializer(task, many=False).data
        return Response(data={
            'result': data,
            'status': 'OK'
        }, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        TaskService.update(
            id=kwargs.get('pk'),
            title=serializer.validated_data['title'],
            description=serializer.validated_data.get('description', None),
            completion_date=serializer.validated_data['completion_date'],
            priority=serializer.validated_data['priority'],
            category_id=serializer.validated_data['category_id'],
            is_done=serializer.validated_data.get('is_done', False),
            completed_at=serializer.validated_data.get('completed_at', None),
            comment=serializer.validated_data.get('comment', None),
            user=request.user
        )
        return Response(data={
            'message': 'The task has been successfully updated',
            'status': 'OK'
        }, status=status.HTTP_200_OK)
    
    def destroy(self, request, *args, **kwargs):
        TaskService.delete(id=kwargs.get('pk'))
        return Response(data={
            'message': 'The task has been successfully deleted',
            'status': 'NO CONTENT'
        }, status=status.HTTP_204_NO_CONTENT)


class FileViewSet(ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = FileSerializer
    lookup_field = 'pk'
    queryset = TaskFile.objects.all()

    def download(self, request, *args, **kwargs):
        file = FileService.get(id=kwargs.get('pk'))
        filename = file.file.name.split('/')[-1]
        response = HttpResponse(file.file, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response
    
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        FileService.create(
            task_id=kwargs.get('task_pk'),
            file=serializer.validated_data.get('file')
        )
        return Response(data={
            'message': 'The file has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)
