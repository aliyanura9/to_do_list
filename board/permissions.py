from rest_framework.permissions import BasePermission
from rest_framework.exceptions import PermissionDenied
from board.models import Category, Task


class IsCategoryOwner(BasePermission):
    def has_permission(self, request, view):
        category = Category.objects.filter(id=view.kwargs.get('pk'))
        if category.first():
            if category.first().user == request.user:
                return True
        raise PermissionDenied
