from django.urls import path
from board.views import CategoryViewSet, TaskViewSet,\
                        FileViewSet


urlpatterns = [
    path('categories', CategoryViewSet.as_view({"get": 'list', "post": 'create'}), name='categories'),
    path('category/<int:pk>', CategoryViewSet.as_view({"put": 'update', 'delete': 'destroy'}), name='category'),
    path('task', TaskViewSet.as_view({'get': 'list', 'post': 'create'}), name='tasks'),
    path('task/<int:pk>', TaskViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='task'),
    path('file/create/<int:task_pk>', FileViewSet.as_view({'post': 'create'}), name='file-create'),
    path('file/<int:pk>', FileViewSet.as_view({'get': 'download', 'delete': 'destroy'}), name='file')
]
