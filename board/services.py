from datetime import datetime, timedelta, timezone
from board.models import Category, Task, TaskFile
from users.exceptions import ObjectNotFoundException
from to_do_list.tasks import app, notify_user_about_task


class CategoryService:
    model = Category

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')
        
    @classmethod
    def create(cls, user, title):
        cls.model.objects.create(title=title, user=user)


class TaskService:
    model = Task

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')

    @classmethod
    def get_notify_time(cls, completion_date):
        date = None
        tz = timezone(offset=timedelta(hours=6), name='Asia/Bishkek')
        if (datetime.now(tz) + timedelta(days=1)) < completion_date:
            date = completion_date - timedelta(days=1)
        elif completion_date > (datetime.now(tz) + timedelta(hours=1)):
            date = completion_date - timedelta(hours=1)
        elif completion_date > datetime.now(tz):
            date = datetime.now(tz)+timedelta(minutes=1)
        
        return date

    @classmethod
    def create(cls, title: str, description: str,
               priority: str, completion_date:str, 
               category_id: int, comment:str, user):
        category = CategoryService.get(id=category_id)
        task = cls.model.objects.create(
            title=title,
            description=description,
            priority=priority,
            completion_date=completion_date,
            category=category,
            commemt = comment
        )
        date = cls.get_notify_time(task.completion_date)
        if date:
            celery_task = notify_user_about_task.apply_async(args=[task.title, user.email], eta=date)
        task.celery_task_id = celery_task.id
        task.save()      

    @classmethod
    def update(cls, id: int, title: str, description: str,
               priority: str, completion_date: str, 
               is_done: bool, completed_at: str, comment: str,
               category_id: int, user):
        category = CategoryService.get(id=category_id)
        task = cls.get(id=id)
        task.title = title
        task.description = description
        task.priority = priority
        task.category = category
        task.comment = comment
        if task.completion_date != completion_date:
            app.control.revoke(task.celery_task_id)
            task.completion_date = completion_date
            date = cls.get_notify_time(completion_date)
            celery_task = notify_user_about_task.apply_async(args=[title, user.email], eta=date)
            task.celery_task_id = celery_task.id
        if task.is_done != is_done:
            if is_done == False:
                task.completed_at = None
        if is_done == True:
            tz = timezone(offset=timedelta(hours=6), name='Asia/Bishkek')
            if completed_at:
                task.completed_at = completed_at
            else:
                task.completed_at = datetime.now(tz)
        task.save()
        
    @classmethod
    def delete(cls, id):
        task = cls.get(id=id)
        app.control.revoke(task.celery_task_id)
        task.delete()


class FileService:
    model = TaskFile

    @classmethod
    def get(cls, *args, **kwargs) -> model:
        try:
            return cls.model.objects.get(*args, **kwargs)
        except cls.model.DoesNotExist:
            raise ObjectNotFoundException(f'{cls.model.__name__} not found')

    @classmethod
    def create(cls, task_id: int, file):
        task = TaskService.get(id=task_id)
        cls.model.objects.create(
            task=task,
            file=file
        )
