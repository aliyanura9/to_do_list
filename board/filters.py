from django_filters.rest_framework import FilterSet, BaseInFilter,\
                                          DateTimeFilter
from board.models import Task


class TaskFilter(FilterSet):
    '''Manga filtering by year of publication'''

    completion_date_start = DateTimeFilter(field_name='completion_date', lookup_expr='gte')
    completion_date_end = DateTimeFilter(field_name='completion_date', lookup_expr='lte')
    category = BaseInFilter(
        field_name='category__id',
        lookup_expr='in',
        distinct=True
    )

    class Meta:
        model = Task
        fields = ['completion_date_start', 'completion_date_end', 'category']
