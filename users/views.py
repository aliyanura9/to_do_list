from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet
from users.serializers import RegistrationSerializer,\
                              LoginSerializer
from users.services import UserService, TokenService


class RegistrationViewSet(ModelViewSet):
    """User registration view"""

    serializer_class = RegistrationSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = UserService.create_user(
            email=serializer.validated_data.get('email'),
            username=serializer.validated_data.get('username'),
            password=serializer.validated_data.get('password'),
            password2=serializer.validated_data.get('password2'),
        )
        return Response(data={
            'message': 'The user has successfully registered and the profile has been successfully created',
            'status': 'CREATED'
        }, status=status.HTTP_201_CREATED)


class LoginViewSet(ModelViewSet):
    """User login view"""

    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = TokenService.create_auth_token(
            username=serializer.validated_data.get('username'),
            password=serializer.validated_data.get('password')
        )
        return Response(data={
            'message': 'You have successfully logged in',
            'data': {
                'token': str(token),
                'token_type': 'Token',
                'user_id': user.pk,
            },
            'status': "OK"
        }, status=status.HTTP_200_OK)
