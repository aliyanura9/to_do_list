FROM python:3.10

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /to_do_list
WORKDIR /to_do_list

COPY requirements.txt /to_do_list/
COPY . /to_do_list/

RUN pip install -r requirements.txt

RUN mkdir -p /media
RUN mkdir -p /staticfiles

